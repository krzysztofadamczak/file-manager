import Router from '../utils/router/router';
import Route from '../utils/router/route';
import StoragePage from '../pages/storage/storage.page';
import HomePage from '../pages/home/home.page';

const router = new Router ([
  new Route ('', HomePage),
  new Route ('home', HomePage),
  new Route ('storage', StoragePage),
]);

export default router;
