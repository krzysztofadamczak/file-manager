export default class Route {
  constructor (path, template) {
    this.path = path;
    this.template = template;
  }
}
