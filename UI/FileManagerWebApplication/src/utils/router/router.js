export default class Router {
  constructor (routes) {
    this.routes = routes;

    this.initHistoryHandler ();
  }

  initHistoryHandler () {
    window.onpopstate = function () {
      Router.go (document.location.pathname);
    };
  }

  static go (path) {
    history.pushState ({}, document.title, path);

    const app = document.querySelector ('#app');

    while (app.hasChildNodes ()) {
      app.removeChild (app.childNodes[0]);
    }

    app.append (
      new (window.Router.routes.find (
        route => route.path === path || `/${route.path}` === path
      ).template) ()
    );
  }

  static initBase () {
    history.replaceState ({}, document.title, document.location.href);

    Router.go (document.location.pathname);
  }
}
