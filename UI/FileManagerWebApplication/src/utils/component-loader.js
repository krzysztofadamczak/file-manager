export default (function (html, styles) {
  const template = document.createElement ('template');

  template.innerHTML = `
  <style>
  ${styles}
  </style>
  ${html}
`;

  return template;
});
