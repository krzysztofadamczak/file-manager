import html from './home.page.html';

const template = document.createElement ('template');
template.innerHTML = html;

export default class HomePage extends HTMLElement {
  constructor () {
    super ();

    this._shadowRoot = this.attachShadow ({mode: 'open'});
    this._shadowRoot.appendChild (template.content.cloneNode (true));
  }
}

window.customElements.define ('app-home-page', HomePage);
