import html from './storage.page.html';

const template = document.createElement ('template');
template.innerHTML = html;

export default class StoragePage extends HTMLElement {
  constructor () {
    super ();

    this._shadowRoot = this.attachShadow ({mode: 'open'});
    this._shadowRoot.appendChild (template.content.cloneNode (true));
  }
}

window.customElements.define ('app-storage-page', StoragePage);
