import './components/header/header.component';
import './components/footer/footer.component';
import './components/terminal/terminal.component';

import router from './configs/router-config';
import Router from './utils/router/router';

require ('./styles.css');
require ('./styles/dark-orange.template.css');

(function () {
  window.Router = router;
  Router.initBase ();
}) ();
