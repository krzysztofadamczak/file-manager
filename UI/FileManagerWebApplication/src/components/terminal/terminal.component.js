import html from './terminal.component.html';
import styles from './terminal.component.scss';
import componentLoader from '../../utils/component-loader';

const template = componentLoader (html, styles);

export default class TerminalComponent extends HTMLElement {
  lines = [];

  currentLine = '';

  constructor () {
    super ();

    this._shadowRoot = this.attachShadow ({mode: 'open'});
    this._shadowRoot.appendChild (template.content.cloneNode (true));

    this.initTerminal ();
  }

  initTerminal () {
    const terminal = this._shadowRoot.querySelector ('.terminal');

    terminal.addEventListener ('keydown', event => {
      if (event.keyCode >= 37 && event.keyCode <= 40) {
        event.preventDefault ();
      }

      if (event.keyCode === 13) {
        this.lines.push (this.currentLine);
        this.currentLine = '';
        event.preventDefault ();
      } else if (event.keyCode === 8) {
        if (this.currentLine.length > 0) {
          this.currentLine = this.currentLine.slice (0, -1);
        } else {
          event.preventDefault ();
        }
      } else if (event.keyCode >= 32) {
        this.currentLine += event.key;
      }
    });

    terminal.addEventListener ('keyup', event => {
      if (event.keyCode === 13) {
        this.runCommand (this.lines[this.lines.length - 1], terminal);
        const restore = this.saveCaretPosition (terminal);
        terminal.innerHTML += '<br/>$>&nbsp;';
        restore (3);
      }
    });
  }

  runCommand (command, terminal) {
    if (command === 'SHOW ALL') {
      const restore = this.saveCaretPosition (terminal);
      const text = 'a';
      terminal.innerHTML += '<br/>' + text;
      restore (text.length);
    } else {
      const restore = this.saveCaretPosition (terminal);
      const text = 'UNKNOWN COMMAND';
      terminal.innerHTML += '<br/><span style="color: red">' + text + '</span>';
      restore (text.length);
    }
  }

  saveCaretPosition (context) {
    const selection = this._shadowRoot.getSelection ();
    const range = selection.getRangeAt (0);
    range.setStart (context, 0);
    const len = range.toString ().length;

    const getTextNodeAtPosition = this.getTextNodeAtPosition;

    return function restore (nextLen) {
      const pos = getTextNodeAtPosition (context, len);
      selection.removeAllRanges ();
      const range = new Range ();
      range.setStart (pos.node, pos.position + nextLen);
      selection.addRange (range);
    };
  }

  getTextNodeAtPosition (root, index) {
    const treeWalker = document.createTreeWalker (
      root,
      NodeFilter.SHOW_TEXT,
      function next (elem) {
        if (index >= elem.textContent.length) {
          index -= elem.textContent.length;
          return NodeFilter.FILTER_REJECT;
        }
        return NodeFilter.FILTER_ACCEPT;
      }
    );

    const c = treeWalker.nextNode ();

    return {
      node: c ? c : root,
      position: c ? index : 0,
    };
  }
}

window.customElements.define ('app-terminal', TerminalComponent);
