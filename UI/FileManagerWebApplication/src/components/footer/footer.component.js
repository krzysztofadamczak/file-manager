import html from './footer.component.html';
import styles from './footer.component.scss';
import loadComponent from '../../utils/component-loader';

const template = loadComponent (html, styles);

export default class FooterTemplate extends HTMLElement {
  isOpen = false;

  constructor () {
    super ();

    this._shadowRoot = this.attachShadow ({mode: 'open'});
    this._shadowRoot.appendChild (template.content.cloneNode (true));

    this.init ();
  }

  init () {
    const container = this._shadowRoot.querySelector ('.container');
    const header = container.querySelector ('.header');

    container.addEventListener ('click', () => {
      if (!this.isOpen) {
        this.isOpen = true;
        this.toggleTerminal (this.isOpen);
      }
    });

    header
      .querySelector ('.close-button')
      .addEventListener ('click', this.headerClickEvent);
  }

  toggleTerminal (isOpen) {
    const container = this._shadowRoot.querySelector ('.container');
    const containerTitle = container.querySelector ('.terminal-title');
    const header = container.querySelector ('.header');
    const content = container.querySelector ('.content');

    if (isOpen) {
      container.style.height = '350px';
      header.style.display = 'flex';
      content.style.display = 'block';
      containerTitle.style.display = 'none';
    } else {
      container.style.height = '20px';
      header.style.display = 'none';
      content.style.display = 'none';
      containerTitle.style.display = 'block';
    }
  }

  headerClickEvent = event => {
    event.stopPropagation ();
    this.isOpen = false;
    this.toggleTerminal (this.isOpen);
  };
}

window.customElements.define ('app-footer', FooterTemplate);
