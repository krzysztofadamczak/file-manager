import html from './header.component.html';
import styles from './header.component.scss';

import Router from '../../utils/router/router';
import loadComponent from '../../utils/component-loader';

const template = loadComponent (html, styles);

const MENU_ITEMS = [
  {
    title: 'Home',
    path: 'home',
  },
  {
    title: 'Storage',
    path: 'storage',
  },
];

export default class Header extends HTMLElement {
  constructor () {
    super ();

    this._shadowRoot = this.attachShadow ({mode: 'open'});
    this._shadowRoot.appendChild (template.content.cloneNode (true));

    this.initElements ();
  }

  initElements () {
    const menuContainer = this._shadowRoot.querySelector ('.menu');

    MENU_ITEMS.forEach (item => {
      const div = document.createElement ('div');
      div.classList.add ('menu-item');

      const span = document.createElement ('span');
      span.textContent = item.title;

      div.appendChild (span);

      div.addEventListener ('click', () => {
        Router.go (item.path);
      });

      menuContainer.appendChild (div);
    });
  }
}

window.customElements.define ('app-header', Header);
